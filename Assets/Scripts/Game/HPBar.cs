﻿using UnityEngine;

public class HPBar : MonoBehaviour {


    public void SetHP(int maxValue, int currentValue)
    {
        transform.localScale = new Vector3((float)currentValue / maxValue, transform.localScale.y);
    }

}
