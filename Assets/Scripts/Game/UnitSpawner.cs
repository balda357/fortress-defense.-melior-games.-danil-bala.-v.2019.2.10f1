﻿using UnityEngine;

public class UnitSpawner : MonoBehaviour {

    public Warrior SpawnUnit(GameObject unitPrefab)
    {
        var unitGO = Instantiate(unitPrefab, transform.position, Quaternion.identity);

        return unitGO.GetComponent<Warrior>();
    }
}
