﻿using UnityEngine;
using UnityEngine.UI;

public class SpawnUnitButton : MonoBehaviour {

    [SerializeField] private Text priceText;
    [SerializeField] private GameObject unitPrefab;

    private int price;

    private void Start()
    {
        price = unitPrefab.GetComponentInChildren<Unit>().stats.price;
        priceText.text = price.ToString();
    }

    private void CheckEnoughMoney()
    {
        
    }

    public void OnButtonClick()
    {
        if (GameController.Instance.GetCoins(price))
            GameController.Instance.SpawnUnit(Unit.Side.my, unitPrefab);
    }
}
