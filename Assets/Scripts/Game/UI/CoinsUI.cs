﻿using UnityEngine;
using UnityEngine.UI;

public class CoinsUI : MonoBehaviour {

    [SerializeField] private Text coinsCountText;


    private void Start()
    {
        ShowCoinsCount(GameController.Instance.coins);
        GameController.OnCoinChanging += ShowCoinsCount;
    }

    private void ShowCoinsCount(int count)
    {
        coinsCountText.text = count.ToString();
    }

}
