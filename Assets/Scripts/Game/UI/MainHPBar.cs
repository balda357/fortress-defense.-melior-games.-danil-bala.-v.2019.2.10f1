﻿using UnityEngine;
using UnityEngine.UI;

public class MainHPBar : MonoBehaviour {

    [SerializeField] private Text hpValueText;
    [SerializeField] private Castle castle;
    [SerializeField] private Image bar;

    private void Start()
    {
        castle.OnHPChanging += ShowHP;
    }

    private void ShowHP(int value)
    {
        hpValueText.text = value + "/" + castle.stats.maxHP;
        bar.fillAmount = (float)value / castle.stats.maxHP ;

    }

}
