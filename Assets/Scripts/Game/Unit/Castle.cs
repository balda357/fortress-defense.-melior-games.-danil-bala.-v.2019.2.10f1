﻿using UnityEngine;

public class Castle : Unit {

    public delegate void ChangingHP(int currentHP);
    public event ChangingHP OnHPChanging;


    protected override void Start()
    {
        base.Start();
    }


    public override void Die()
    {
        base.Die();

        GameController.Instance.ShowResultMenu(stats.side == Side.my);
    }


    public override void ChangeHP(int delta)
    {
        base.ChangeHP(delta);

        if (HealthPoints > 3 * stats.maxHP / 4)
            animator.Play("1");
        else
        if (HealthPoints <= 3 * stats.maxHP / 4 && HealthPoints > 2 * stats.maxHP / 4)
            animator.Play("2");
        else
        if (HealthPoints <= 2 * stats.maxHP / 4 && HealthPoints > stats.maxHP / 4)
            animator.Play("3");
        else
        if (HealthPoints <= stats.maxHP / 4)
            animator.Play("4");


        OnHPChanging(HealthPoints);
    }

}
