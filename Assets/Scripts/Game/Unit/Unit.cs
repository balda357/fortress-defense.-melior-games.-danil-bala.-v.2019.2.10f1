﻿using UnityEngine;

public class Unit : MonoBehaviour, IDamagable {


    public Stats stats;

    protected State state = State.Walk;
    protected Animator animator;

    public delegate void OnDestroyUnit();
    public event OnDestroyUnit OnUnitDestroy;

    public int HealthPoints { get; set; }

    protected bool isDead = false;


    public virtual void ChangeHP(int delta)
    {
        if (delta < 0)
            delta = (int)(delta * (1f - (float)stats.armour/10));//расчет урона с броней

        if (HealthPoints + delta < 0) //Если убили
        {
            HealthPoints = 0;
            Die();
            return;
        }
        else
            if (HealthPoints + delta > stats.maxHP)//Если захилили на максимум
        {
            Heal(stats.maxHP - HealthPoints);
            HealthPoints = stats.maxHP;
            return;
        }


        if (delta < 0)//Если принимаем урон
        {
            TakeDamage(delta);
            HealthPoints += delta;
        }
        else if (delta > 0)//Если просто хилим
        {
            Heal(delta);
            HealthPoints += delta;
        }
    }


    public virtual void Heal(int delta)
    {

    }

    public virtual void TakeDamage(int delta)
    {

    }

    public virtual void Die()
    {
        isDead = true;

        animator.Play("Die");

        if (OnUnitDestroy != null)
            OnUnitDestroy();

        Destroy(transform.parent.gameObject, 0.5f);
    }



    protected virtual void Start()
    {
        HealthPoints = stats.maxHP;

        animator = GetComponent<Animator>();
    }


    protected virtual void FixedUpdate()
    {
        if (isDead) return;

        switch (state)
        {
            case State.Walk:
                if (stats.side == Side.my)
                    transform.parent.position += Vector3.right * stats.speed * 0.01f * Time.deltaTime;
                else
                    transform.parent.position += Vector3.left * stats.speed * 0.01f * Time.deltaTime;
                break;

            case State.Attack:

                break;
        }
    }


    [System.Serializable]
    public struct Stats
    {
        public int damage;
        public int maxHP;
        public int armour;
        public int price;
        public int speed;
        [Range (1, 10)]
        public int atkSpeed;
        public Side side;


    }
    public enum Side
    {
        my,
        enemy
    }
    public enum State
    {
        Stay,
        Walk,
        Attack
    }
}

