﻿
public interface IDamagable {

    int HealthPoints { get; set; }

    /// <summary>
    /// Меняет хп
    /// </summary>
    /// <param name="delta"> отрицательное, если принимает урон, положительное, ели хилится </param>
    void ChangeHP(int delta);

    void Heal(int delta);

    void TakeDamage(int delta);

    void Die();
}
