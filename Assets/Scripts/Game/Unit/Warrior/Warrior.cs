﻿using UnityEngine;

public class Warrior : Unit, IHPBarable {

    [SerializeField] private GameObject hpBarPrefab;
    [SerializeField] private float atkDistance;

    [HideInInspector] public Unit unitToAttack;
    private HPBar hpBar;

    
    private void DetectUnit()
    {
        RaycastHit2D hit;

        if (stats.side == Side.my)
        {
            hit = Physics2D.Raycast(transform.position, Vector2.right, 10, 1);

            if (hit.rigidbody == null || hit.rigidbody.tag != "Enemy")
            {
                StartWalk();
                return;
            }
        }
        else return;

        Transform aimTransform = hit.rigidbody.transform;
        float distance = Mathf.Abs(aimTransform.position.x - transform.position.x);
        
        if (distance <= atkDistance)
        {
            unitToAttack = aimTransform.gameObject.GetComponent<Unit>();
            StartAttack(unitToAttack);
        }
        else
            StartWalk();
    }

    protected override void FixedUpdate()
    {
        base.FixedUpdate();

        DetectUnit();
    }


    public override void ChangeHP(int delta)
    {
        base.ChangeHP(delta);

        hpBar.SetHP(stats.maxHP, HealthPoints);
    }

    public void SpawnHPBar()
    {
        hpBar = Instantiate(hpBarPrefab, new Vector3(transform.position.x, transform.position.y + 1.5f), Quaternion.identity, transform.parent).GetComponent<HPBar>();
        hpBar.SetHP(stats.maxHP, HealthPoints);
    }


    protected override void Start()
    {
        base.Start();

        SpawnHPBar();
    }

    /// <summary>
    /// Use as trigger on attack animation end
    /// </summary>
    public void Attack()
    {
        if (unitToAttack != null)
        {
            unitToAttack.ChangeHP(-stats.damage);
        }
    }

    private void ResetDetectedUnit()
    {
        if (unitToAttack != null)
        {
            unitToAttack.OnUnitDestroy -= ResetDetectedUnit;

            unitToAttack = null;
        }
        StartAttack(null);
    }

    protected virtual void StartWalk()
    {
        if (isDead) return;

        if (animator == null) animator = GetComponent<Animator>();
        animator.Play("Walk");

        state = State.Walk;
    }

    protected virtual void StartAttack(Unit unitToAttack)
    {
        if (unitToAttack == null)
        {
            StartWalk();
            return;
        }

        unitToAttack.OnUnitDestroy += ResetDetectedUnit;

        animator.Play("Attack");

        this.unitToAttack = unitToAttack;

        state = State.Attack;
    }
}
