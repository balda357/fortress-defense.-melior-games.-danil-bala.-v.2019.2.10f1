﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Warrior {


    private void OnTriggerStay2D(Collider2D collision)
    {
        if (unitToAttack == null)
        {
            if (collision.tag == "My")
            {
                unitToAttack = collision.gameObject.GetComponent<Unit>();
                StartAttack(unitToAttack);
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        unitToAttack = null;
        StartWalk();
    }

}
