﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

    [SerializeField] private UnitSpawner myUnitSpawner;
    [SerializeField] private UnitSpawner enemyUnitSpawner;

    [SerializeField] private GameObject resultWindow;
    [SerializeField] private Text textResultWindow;


    [SerializeField] private List<GameObject> enemiesPrefabs;

    public delegate void ChangeCoins(int count);
    public static event ChangeCoins OnCoinChanging;


    public int coins = 10;

    #region SINGLETON
    private static GameController _instance;
    public static GameController Instance
    {
        get {
            if (_instance == null) _instance = FindObjectOfType<GameController>();
            return _instance;
        }
    }
    #endregion

    private bool isGameOver = false;

    private List<Warrior> myWarriors;
    private List<Warrior> enemyWarriors;

    private void Start()
    {
        myWarriors = new List<Warrior>();
        enemyWarriors = new List<Warrior>();

        StartGame();
    }

    public void AddCoins(int count)
    {
        coins += count;
        OnCoinChanging?.Invoke(coins);
    }

    public bool CheckEnoughMoney(int price)
    {
        if (price > coins)
            return false;

        else
            return true;
    }

    public bool GetCoins(int price)
    {
        if (!CheckEnoughMoney(price))
            return false;

        coins -= price;
        OnCoinChanging?.Invoke(coins);

        return true;
    }

    public void SpawnUnit(Unit.Side side, GameObject prefab)
    {
        if (side == Unit.Side.my)
        {
            myWarriors.Add(myUnitSpawner.SpawnUnit(prefab));
        }
        else
        {
            enemyWarriors.Add(enemyUnitSpawner.SpawnUnit(prefab));
        }
    }

    private void StartGame()
    {
        StartCoroutine(CoinsAdder());
        StartCoroutine(EnemySpawning());
    }


    private IEnumerator EnemySpawning()
    {
        while (!isGameOver)
        {
            yield return new  WaitForSeconds(5f);

            SpawnUnit(Unit.Side.enemy, enemiesPrefabs[Random.Range(0, enemiesPrefabs.Count)]);

            yield return new WaitForSeconds(5f);

        }
    }

    private IEnumerator CoinsAdder()
    {
        while (!isGameOver)
        {
            yield return new WaitForSeconds(5f);

            AddCoins(1);
        }
    }

    public void ShowResultMenu(bool isEnemyWin)
    {
        resultWindow.SetActive(true);

        if (isEnemyWin)
        {
            textResultWindow.text = "ENEMY WIN";
        }
        else
            textResultWindow.text = "YOU WIN";
    }

    public void OnMainMenuButtonClick()
    {
        SceneManager.LoadScene("MainMenu");
    }

    private void OnDestroy()
    {
        OnCoinChanging = null;
    }
}
