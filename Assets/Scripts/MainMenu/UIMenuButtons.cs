﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class UIMenuButtons : MonoBehaviour {

	public void OnStartGameBtnClick()
    {
        SceneManager.LoadScene("Game");
    }

    public void OnQuitGameBtnClick()
    {
        Application.Quit();
    }
}
